//
//  TareasManager.m
//  TareasSQLite
//
//  Created by Máster Móviles on 21/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "TareasManager.h"
#import "Tarea.h"

@implementation TareasManager

    - (NSMutableArray *) listarTareas{
        NSString *querySQL = @"SELECT * FROM tareas ORDER BY vencimiento ASC";
        sqlite3_stmt *statement;
        NSMutableArray *lista = [[NSMutableArray alloc] init];
        
        int result = sqlite3_prepare_v2(db, [querySQL UTF8String], -1,
                                        &statement, NULL);
        if (result==SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int ident = (int) sqlite3_column_int(statement, 0);
                char *titulo = (char *) sqlite3_column_text(statement, 1);
                int prioridad = (int) sqlite3_column_int(statement, 2);
                int unix_time = (int) sqlite3_column_int(statement, 3);
                Tarea *tarea = [[Tarea alloc] init];
                tarea.id = (NSInteger)ident;
                tarea.titulo = [[NSString alloc] initWithUTF8String: titulo];
                tarea.prioridad = (NSInteger)prioridad;
                tarea.vencimiento = [[NSDate alloc] initWithTimeIntervalSince1970: unix_time];
                [lista addObject:tarea];
            }
            
        }
        sqlite3_finalize(statement);
        return lista;
    }

    - (BOOL) insertarTarea: (Tarea*) tarea{
        NSString *querySQL = @"INSERT INTO tareas (titulo, prioridad, vencimiento) VALUES (?,?,?)";
        sqlite3_stmt *statement;
        sqlite3_prepare_v2(db, [querySQL UTF8String], -1, &statement, NULL);
        sqlite3_bind_text(statement, 1, [tarea.titulo UTF8String], -1, SQLITE_STATIC);
        sqlite3_bind_int(statement, 2, (int)tarea.prioridad);
        sqlite3_bind_int(statement, 3, [tarea.vencimiento timeIntervalSince1970]);
        int result = sqlite3_step(statement);
        if (result==SQLITE_DONE){
            NSLog(@"Registro almacenado OK");
            return YES;
        }
        else
            return NO;
    }

@end
