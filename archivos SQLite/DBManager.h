//
//  SQLiteManager.h
//  PruebaSQLite
//
//  Created by Otto Colomina Pardo on 31/12/14.
//  Copyright (c) 2014 Universidad de Alicante. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject
{
  sqlite3 *db;
}

- (id) initWithDB:(NSString *)nombre reload:(BOOL)reload;

@end
