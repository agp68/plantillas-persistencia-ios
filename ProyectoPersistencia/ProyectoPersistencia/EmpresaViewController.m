//
//  EmpresaViewController.m
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "EmpresaViewController.h"
#import "AppDelegate.h"
#import "Empresa.h"

@interface EmpresaViewController ()

@end

@implementation EmpresaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titulo.textColor = [UIColor grayColor];
    self.campoMensaje.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)borrarCampos:(id)sender {
    self.nombre_empresa.text = @"";
    self.cif_empresa.text = @"";
    self.localidad_empresa.text = @"";
    self.campoMensaje.text = @"";
}

- (IBAction)guardaCampos:(id)sender {
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    Empresa *e = [NSEntityDescription insertNewObjectForEntityForName:@"Empresa"
                                               inManagedObjectContext:miContexto];
    e.nombre = self.nombre_empresa.text;
    e.cif = self.cif_empresa.text;
    e.localidad = self.localidad_empresa.text;
    
    NSError *error;
    [miContexto save:&error];
    
    if (!error) {
        self.campoMensaje.text = @"Empresa guardada";
        self.campoMensaje.textColor = [UIColor greenColor];
    }
    else {
        self.campoMensaje.textColor = [UIColor redColor];
        self.campoMensaje.text = error.description;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
