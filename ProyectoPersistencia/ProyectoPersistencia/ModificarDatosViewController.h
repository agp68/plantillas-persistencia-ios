//
//  ModificarDatosViewController.h
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 04/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Empleado.h"

@interface ModificarDatosViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *editarNombre;
@property (weak, nonatomic) IBOutlet UITextField *editarNif;
@property (weak, nonatomic) IBOutlet UITextField *editarLocalidad;
@property (weak, nonatomic) IBOutlet UITextField *editarPuesto;

@property (weak, nonatomic) IBOutlet UILabel *textoNombre;
@property (weak, nonatomic) IBOutlet UILabel *textoCif;
@property (weak, nonatomic) IBOutlet UILabel *textoLocalidad;

@property (weak, nonatomic) IBOutlet UILabel *campoMensaje;

@property Empleado *empleado;
@end
