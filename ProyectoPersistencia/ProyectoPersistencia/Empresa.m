//
//  Empresa.m
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "Empresa.h"


@implementation Empresa

@dynamic cif;
@dynamic nombre;
@dynamic localidad;
@dynamic relempleados;

@end
