//
//  EmpresaViewController.h
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmpresaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *nombre_empresa;
@property (weak, nonatomic) IBOutlet UITextField *cif_empresa;
@property (weak, nonatomic) IBOutlet UITextField *localidad_empresa;
@property (weak, nonatomic) IBOutlet UILabel *campoMensaje;
@property (weak, nonatomic) IBOutlet UILabel *titulo;

@end
