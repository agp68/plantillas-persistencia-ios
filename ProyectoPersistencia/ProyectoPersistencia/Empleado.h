//
//  Empleado.h
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Empresa;

@interface Empleado : NSManagedObject

@property (nonatomic, retain) NSString * localidad;
@property (nonatomic, retain) NSString * nif;
@property (nonatomic, retain) NSString * nombre;
@property (nonatomic, retain) NSString * puesto;
@property (nonatomic, retain) Empresa *relempresa;

@end
