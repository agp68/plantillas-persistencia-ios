//
//  Empleado.m
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "Empleado.h"
#import "Empresa.h"


@implementation Empleado

@dynamic localidad;
@dynamic nif;
@dynamic nombre;
@dynamic puesto;
@dynamic relempresa;

@end
