//
//  Empresa.h
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Empresa : NSManagedObject

@property (nonatomic, retain) NSString * cif;
@property (nonatomic, retain) NSString * nombre;
@property (nonatomic, retain) NSString * localidad;
@property (nonatomic, retain) NSSet *relempleados;
@end

@interface Empresa (CoreDataGeneratedAccessors)

- (void)addRelempleadosObject:(NSManagedObject *)value;
- (void)removeRelempleadosObject:(NSManagedObject *)value;
- (void)addRelempleados:(NSSet *)values;
- (void)removeRelempleados:(NSSet *)values;

@end
