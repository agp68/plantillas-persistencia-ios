//
//  EmpleadosViewController.m
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "EmpleadosViewController.h"
#import "AppDelegate.h"
#import "Empresa.h"
#import "Empleado.h"

@interface EmpleadosViewController ()

@end

@implementation EmpleadosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.campoMensaje.text = @"";
}

- (void)viewDidAppear:(BOOL)animated{
    // Do any additional setup after loading the view.
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Empresa"];
    self.empresa = [miContexto
                    executeFetchRequest:fetchRequest
                    error:nil];
    self.combo.dataSource = self;
    self.combo.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// The number of columns of data
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.empresa count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Empresa *e = self.empresa[row];
    return e.nombre;
}

- (IBAction)borrarCamposEmpleado:(id)sender {
    self.nombre_empleado.text = @"";
    self.nif_empleado.text = @"";
    self.localidad_empleado.text = @"";
    self.puesto_empleado.text = @"";
    self.campoMensaje.text = @"";
}

- (IBAction)guardarCamposEmpleado:(id)sender {
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    Empleado *e = [NSEntityDescription insertNewObjectForEntityForName:@"Empleado"
                                               inManagedObjectContext:miContexto];
    e.nombre = self.nombre_empleado.text;
    e.nif = self.nif_empleado.text;
    e.localidad = self.localidad_empleado.text;
    e.puesto = self.puesto_empleado.text;
    
    NSInteger index = [self.combo selectedRowInComponent:0];
    e.relempresa = [self.empresa objectAtIndex:index];
    
    NSError *error;
    [miContexto save:&error];
    
    if (!error) {
        self.campoMensaje.textColor = [UIColor greenColor];
        self.campoMensaje.text = @"Empleado guardado";
    }
    else {
        self.campoMensaje.textColor = [UIColor redColor];
        self.campoMensaje.text = error.description;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
