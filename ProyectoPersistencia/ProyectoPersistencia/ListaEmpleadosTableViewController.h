//
//  ListaEmpleadosTableViewController.h
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface ListaEmpleadosTableViewController : UITableViewController<NSFetchedResultsControllerDelegate,UISearchBarDelegate>
    @property NSFetchedResultsController *frController;
@end
