//
//  EmpleadosViewController.h
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmpleadosViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nombre_empleado;
@property (weak, nonatomic) IBOutlet UITextField *nif_empleado;
@property (weak, nonatomic) IBOutlet UITextField *localidad_empleado;
@property (weak, nonatomic) IBOutlet UITextField *puesto_empleado;
@property (weak, nonatomic) IBOutlet UIPickerView *combo;
@property NSArray *empresa;
@property (weak, nonatomic) IBOutlet UILabel *campoMensaje;

@end
