//
//  ModificarDatosViewController.m
//  ProyectoPersistencia
//
//  Created by Máster Móviles on 04/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "ModificarDatosViewController.h"
#import "Empresa.h"
#import "AppDelegate.h"

@interface ModificarDatosViewController ()

@end

@implementation ModificarDatosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.campoMensaje.text = @"";
    self.editarNombre.text = self.empleado.nombre;
    self.editarNif.text = self.empleado.nif;
    self.editarLocalidad.text = self.empleado.localidad;
    self.editarPuesto.text = self.empleado.puesto;
    
    self.textoNombre.text = self.empleado.relempresa.nombre;
    self.textoCif.text = self.empleado.relempresa.cif;
    self.textoLocalidad.text = self.empleado.relempresa.localidad;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)botonActualizar:(id)sender {
    self.empleado.nombre = self.editarNombre.text;
    self.empleado.nif = self.editarNif.text;
    self.empleado.localidad = self.editarLocalidad.text;
    self.empleado.puesto = self.editarPuesto.text;
    NSError *error;
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    [miContexto save:&error];
    if (!error) {
        self.campoMensaje.textColor = [UIColor greenColor];
        self.campoMensaje.text = @"Empleado modificado";
    }
    else {
        self.campoMensaje.textColor = [UIColor redColor];
        self.campoMensaje.text = error.description;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
