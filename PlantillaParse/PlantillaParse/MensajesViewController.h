//
//  MensajesViewController.h
//  PlantillaParse
//
//  Created by Otto Colomina Pardo on 21/1/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MensajesViewController : UIViewController <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *miTableView;
@property (weak, nonatomic) IBOutlet UITextField *campoMensaje;

@end
