//
//  MensajesViewController.m
//  PlantillaParse
//
//  Created by Otto Colomina Pardo on 21/1/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

#import "MensajesViewController.h"
#import <Parse/Parse.h>

@interface MensajesViewController ()

@property NSArray *mensajes;

@end

@implementation MensajesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self obtenerMensajes];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)pulsadoIntroEnMensaje:(id)sender {
    NSLog(@"El mensaje es: %@", self.campoMensaje.text);
    PFObject *obj = [PFObject objectWithClassName:@"Mensaje"];
    obj[@"Texto"] = self.campoMensaje.text;
    obj[@"Usuario"] = [PFUser currentUser];
    [obj saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded)
            NSLog(@"¡Ahora sí sé que se ha guardado!");
        else
            NSLog(@"Pues no, ha habido un error: %@", error.description);
    }];
}

- (IBAction)clickBotonLogout:(id)sender {
    NSLog(@"Se ha pulsado el botón 'Logout'");
    [PFUser logOut];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)clickBotonRefresh:(id)sender {
    NSLog(@"Se ha pulsado el botón 'Refresh'");
    [self obtenerMensajes];
}

- (void) obtenerMensajes {
    PFQuery *query = [PFQuery queryWithClassName:@"Mensaje"];
    [query orderByAscending:@"createdAt"];
    [query includeKey:@"Usuario"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *results, NSError *error) {
        if (!error) {
            NSLog(@"Encontrados %lu alumnos", (unsigned long)[results count]);
            for (PFObject *object in results) {
                NSLog(@"%@", object.objectId);
            }
            self.mensajes = results;
            [self.miTableView reloadData];
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.mensajes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = self.mensajes[indexPath.row][@"Usuario"][@"username"];
    cell.detailTextLabel.text = self.mensajes[indexPath.row][@"Texto"];
    return cell;
}


@end
