//
//  ListaNotasControllerTableViewController.h
//  MisNotas
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListaNotasControllerTableViewController : UITableViewController <UISearchBarDelegate>
    @property NSArray *notas;
@end
