//
//  ListaNotasControllerTableViewController.m
//  MisNotas
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "ListaNotasControllerTableViewController.h"
#import "AppDelegate.h"

@interface ListaNotasControllerTableViewController ()

@end

@implementation ListaNotasControllerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated{
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest
                                    fetchRequestWithEntityName:@"Nota"];
    NSError *error;
    self.notas = [miContexto executeFetchRequest:fetchRequest
                                           error:&error];
    //refrescar la tabla, si no no aparecerán los datos
    [self.tableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectModel *miModelo = [miDelegate managedObjectModel];
    //Buscamos 'Alicante', 'Alacant', 'Candanchú'....
    NSDictionary *dict = @{ @"subcadena" : searchBar.text};
    NSFetchRequest *query = [miModelo
                             fetchRequestFromTemplateWithName:@"buscarNotas"
                             substitutionVariables:dict];
    
    NSSortDescriptor *fechaSort = [NSSortDescriptor
                                   sortDescriptorWithKey:@"fecha" ascending:NO];
    query.sortDescriptors = @[fechaSort];
    
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    NSError *error;
    self.notas = [miContexto executeFetchRequest:query
                                           error:&error];
    
    //Este NSLog es solo para probar!!!!
    //como se ve, "text" es lo escrito en la barra de búsqueda
    NSLog(@"Buscando %@", searchBar.text);
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [self.notas count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [self.notas[indexPath.row] valueForKey:@"texto"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *dateString = [formatter stringFromDate:[self.notas[indexPath.row] valueForKey:@"fecha"]];
    
    cell.detailTextLabel.text = dateString;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
