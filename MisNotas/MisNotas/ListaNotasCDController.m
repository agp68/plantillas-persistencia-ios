//
//  ListaNotasCDController.m
//  MisNotas
//
//  Created by Máster Móviles on 02/02/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "ListaNotasCDController.h"
#import "AppDelegate.h"

@interface ListaNotasCDController ()

@end

@implementation ListaNotasCDController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Nota"];
    NSSortDescriptor *orden = [[NSSortDescriptor alloc ]
                               initWithKey:@"fecha" ascending:NO];
    request.sortDescriptors = @[orden];
    
    //Tenemos una @property NSFetchedResultsController *frController
    self.frController = [[NSFetchedResultsController alloc]
                         initWithFetchRequest:request
                         managedObjectContext:miContexto
                         //Esto por el momento no lo usamos
                         sectionNameKeyPath:nil
                         cacheName:@"miCache2"];
    [self.frController performFetch:nil];
    
    //En el viewDidLoad, tras crear el fetched results controller
    self.frController.delegate = self;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //Borramos el objeto gestionado
        NSManagedObject *nota = [self.frController objectAtIndexPath:indexPath];
        [self.frController.managedObjectContext deleteObject:nota];
        NSError *error;
        [[self.frController managedObjectContext] save:&error];
        if (error) {
            NSLog(@"Error al intentar borrar objeto");
        }
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return [[self.frController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    id<NSFetchedResultsSectionInfo> sectionInfo = [self.frController sections][section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellLista" forIndexPath:indexPath];
    
    NSManagedObject *nota = [self.frController objectAtIndexPath:indexPath];
    
    cell.textLabel.text = [nota valueForKey:@"texto"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *dateString = [formatter stringFromDate:[nota valueForKey:@"fecha"]];
    
    cell.detailTextLabel.text = dateString;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
