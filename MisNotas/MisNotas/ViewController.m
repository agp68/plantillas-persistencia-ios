//
//  ViewController.m
//  MisNotas
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fechaHora.text = @"";
    self.textoNota.text = @"Escribir...";
    self.campoMensaje.text = @"";
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)crearNota:(id)sender {
    self.fechaHora.text = @"";
    self.textoNota.text = @"Escribir...";
    self.campoMensaje.text = @"";
}

- (IBAction)guardarNota:(id)sender {
    //obtenemos el delegate, ya que es donde está el código de acceso a Core Data
    AppDelegate *miDelegate = [[UIApplication sharedApplication] delegate];
    
    //Para crear objetos persistentes necesitamos el contexto
    NSManagedObjectContext *miContexto = [miDelegate managedObjectContext];
    //Vamos a crear un objeto gestionado por Core Data
    NSManagedObject *nuevaNota = [NSEntityDescription
                                  insertNewObjectForEntityForName:@"Nota"
                                  inManagedObjectContext:miContexto];
    [nuevaNota setValue:[NSDate date] forKey:@"fecha"];
    //suponemos que el "outlet" del campo de texto se llama "campoTexto"
    [nuevaNota setValue:self.textoNota.text forKey:@"texto"];

    NSError *error;
    [miContexto save:&error];
    if (!error) {
        self.campoMensaje.text = @"Nota guardada";
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        
        self.fechaHora.text = dateString;
    }
    else {
        self.campoMensaje.text = error.description;
    }
}


@end
