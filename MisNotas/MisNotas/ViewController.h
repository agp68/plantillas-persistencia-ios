//
//  ViewController.h
//  MisNotas
//
//  Created by Máster Móviles on 26/01/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *fechaHora;
@property (weak, nonatomic) IBOutlet UITextView *textoNota;
@property (weak, nonatomic) IBOutlet UILabel *campoMensaje;

@end

